package com.example.syathish.randomjokes;

import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    Button jokeButton;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        jokeButton = findViewById(R.id.show_button);
        final FragmentManager fragmentManager = getSupportFragmentManager();
        final JokeFragment jokeFragment = new JokeFragment();
        jokeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                jokeFragment.show(fragmentManager,"JOKE_FRAGMENT_TAG");
            }
        });
    }
}
