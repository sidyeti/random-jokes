package com.example.syathish.randomjokes.recyclerView;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.syathish.randomjokes.Joke;
import com.example.syathish.randomjokes.R;

import java.util.List;

public class JokeAdapter extends RecyclerView.Adapter {
    int VIEW_ITEM=0;
    int VIEW_PROG=1;
    OnLoadMoreListener onLoadMoreListener;
    private List<Joke> jokeList;
    private int visibleThreshold = 2;
    private int lastVisibleItem, totalItemCount;
    private boolean loading;
    Context context;

    public JokeAdapter(List<Joke> jokeList,RecyclerView recyclerView,Context context) {
        this.jokeList = jokeList;
        this.context = context;
        if(recyclerView.getLayoutManager() instanceof LinearLayoutManager){
            final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
            recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);
                    totalItemCount = linearLayoutManager.getItemCount();
                    lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
                    if (!loading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                        // End has been reached
                        // Do something
                        if (onLoadMoreListener != null) {
                            onLoadMoreListener.onLoadMore();
                        }
                        loading = true;
                    }
                }
            });
        }
    }
    public void setLoaded(){
        loading = false;
    }
    @Override
    public int getItemViewType(int position) {
        return jokeList.get(position) != null ? VIEW_ITEM : VIEW_PROG;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        RecyclerView.ViewHolder vh=null;
        if(i == VIEW_ITEM){
            View view=LayoutInflater.from(viewGroup.getContext())
                    .inflate(R.layout.joke_card_view,viewGroup,false);
            vh = new JokeViewHolder(view);
        } else {
            View view=LayoutInflater.from(viewGroup.getContext())
                    .inflate(R.layout.progress_item,viewGroup,false);
            vh = new ProgressViewHolder(view);
        }
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {

        if(viewHolder instanceof JokeViewHolder) {
            Joke joke = jokeList.get(i);
            JokeViewHolder jokeViewHolder = (JokeViewHolder) viewHolder;
            jokeViewHolder.punchLineTextView.setText(joke.getPunchLine());
            jokeViewHolder.setupTextView.setText(joke.getSetup());
            jokeViewHolder.typeTextView.setText(joke.getJokeType());
            jokeViewHolder.idTextView.setText(Integer.toString(joke.getId()));
        }
        else if(viewHolder instanceof ProgressViewHolder){
            ProgressViewHolder progressViewHolder = (ProgressViewHolder) viewHolder;
            progressViewHolder.progressBar.setIndeterminate(true);
        }
    }

    @Override
    public int getItemCount() {
        return jokeList.size();
    }

    public void setOnLoadMoreListener(OnLoadMoreListener onLoadMoreListener) {
        this.onLoadMoreListener = onLoadMoreListener;
    }

    public interface OnLoadMoreListener{
        void onLoadMore();
    }
}
