package com.example.syathish.randomjokes.recyclerView;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.example.syathish.randomjokes.R;

import org.w3c.dom.Text;

public class JokeViewHolder extends RecyclerView.ViewHolder {
    TextView idTextView;
    TextView typeTextView;
    TextView setupTextView;
    TextView punchLineTextView;

    public JokeViewHolder(@NonNull View itemView) {
        super(itemView);
        idTextView = itemView.findViewById(R.id.id_text_view);
        typeTextView = itemView.findViewById(R.id.type_text_view);
        setupTextView = itemView.findViewById(R.id.setup_text_view);
        punchLineTextView = itemView.findViewById(R.id.punch_text_view);
    }
}
