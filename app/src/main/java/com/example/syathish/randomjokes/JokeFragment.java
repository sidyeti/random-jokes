package com.example.syathish.randomjokes;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.example.syathish.randomjokes.recyclerView.JokeAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class JokeFragment extends DialogFragment {
    RecyclerView recyclerView;
    JokeAdapter jokeAdapter;
    List<Joke> jokeList;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.joke_content,container);
        recyclerView=rootView.findViewById(R.id.recycler_view);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this.getActivity(),LinearLayoutManager.HORIZONTAL,false);
        recyclerView.setLayoutManager(layoutManager);
        jokeList = new ArrayList<>();
        jokeList.add(null);
        jokeAdapter = new JokeAdapter(jokeList, recyclerView, getContext());
        recyclerView.setAdapter(jokeAdapter);

        getData();
        jokeAdapter.setOnLoadMoreListener(new JokeAdapter.OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
                jokeList.add(null);

                recyclerView.post(new Runnable() {
                    @Override
                    public void run() {
                        jokeAdapter.notifyItemInserted(jokeList.size()-1);
                    }
                });
                getData();
            }
        });
        return rootView;
    }

    void getData(){
        String url = "https://08ad1pao69.execute-api.us-east-1.amazonaws.com/dev/random_ten";
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.GET, url, null, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                jokeList.remove(null);
                jokeAdapter.notifyItemRemoved(jokeList.size());
                try{
                    for(int i=0;i<response.length();i++){
                        JSONObject jokeObject = response.getJSONObject(i);
                        Joke joke = new Joke();
                        joke.setId(jokeObject.getInt("id"));
                        joke.setSetup(jokeObject.getString("setup"));
                        joke.setJokeType(jokeObject.getString("type"));
                        joke.setPunchLine(jokeObject.getString("punchline"));
                        jokeList.add(joke);
                        jokeAdapter.notifyItemInserted(jokeList.size());
                    }
                }
                catch (JSONException e){
                    Toast.makeText(getActivity().getApplicationContext(), "Unable to fetch data", Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
                jokeAdapter.setLoaded();
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("VOLLEY_ERROR_EVENTS",error.toString());
            }
        });
        jsonArrayRequest.setRetryPolicy(new DefaultRetryPolicy(7000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue requestQueue = Volley.newRequestQueue(getContext());
        requestQueue.add(jsonArrayRequest);
    }
}
