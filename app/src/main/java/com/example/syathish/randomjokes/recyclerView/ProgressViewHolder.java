package com.example.syathish.randomjokes.recyclerView;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;

import com.example.syathish.randomjokes.R;

public class ProgressViewHolder extends RecyclerView.ViewHolder {
    ProgressBar progressBar;
    public ProgressViewHolder(@NonNull View itemView) {
        super(itemView);
        progressBar = itemView.findViewById(R.id.progress_bar);

    }
}
